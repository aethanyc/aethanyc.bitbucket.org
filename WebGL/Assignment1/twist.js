// Adapted from http://www.cs.unm.edu/~angel/COURSERA/CODE/EXAMPLES/gasket2.html

"use strict";

var canvas;
var gl;

var points = [];
var numTimesToSubdivide = 5;
var degrees = 30;
var distanceFactor = 2;

function init()
{
  canvas = document.getElementById( "gl-canvas" );
  degrees = parseInt(document.getElementById("degree").value);
  numTimesToSubdivide = parseInt(document.getElementById("step").value);
  distanceFactor = parseInt(document.getElementById("distance").value);

  gl = WebGLUtils.setupWebGL( canvas );
  if ( !gl ) { alert( "WebGL isn't available" ); }

  //
  //  Configure WebGL
  //
  gl.viewport( 0, 0, canvas.width, canvas.height);
  gl.clearColor( 1.0, 1.0, 1.0, 1.0 );

  //  Load shaders and initialize attribute buffers

  var program = initShaders( gl, "vertex-shader", "fragment-shader" );
  gl.useProgram( program );

  // Load the data into the GPU

  var bufferId = gl.createBuffer();
  gl.bindBuffer( gl.ARRAY_BUFFER, bufferId );
  gl.bufferData( gl.ARRAY_BUFFER, 3*2*Math.pow(4, 6), gl.STATIC_DRAW );

  // Associate out shader variables with our data buffer

  var vPosition = gl.getAttribLocation( program, "vPosition" );
  gl.vertexAttribPointer( vPosition, 2, gl.FLOAT, false, 0, 0 );
  gl.enableVertexAttribArray( vPosition );

  document.getElementById("step").onchange = function () {
    numTimesToSubdivide = document.getElementById("step").value;
    render();
  };

  document.getElementById("degree").onchange = function () {
    degrees = document.getElementById("degree").value;
    render();
  };

  document.getElementById("distance").onchange = function () {
    distanceFactor = document.getElementById("distance").value;
    render();
  };

  render();
};

function rotate( vertex )
{
  var x = vertex[0];
  var y = vertex[1];
  var rad = radians(degrees) * Math.sqrt(x*x + y*y) * distanceFactor;
  var new_x = x * Math.cos(rad) - y * Math.sin(rad);
  var new_y = x * Math.sin(rad) + y * Math.cos(rad);

  return vec2(new_x, new_y);
}

function triangle( a, b, c )
{
  points.push( a, b, c );
}

function divideTriangle( a, b, c, count )
{
  // check for end of recursion

  if ( count === 0 ) {
    var ra = rotate(a);
    var rb = rotate(b);
    var rc = rotate(c);
    triangle( ra, rb, rc );
  } else {
    //bisect the sides

    var ab = mix( a, b, 0.5 );
    var ac = mix( a, c, 0.5 );
    var bc = mix( b, c, 0.5 );

    --count;

    // three new triangles

    divideTriangle( a, ab, ac, count );
    divideTriangle( c, ac, bc, count );
    divideTriangle( b, bc, ab, count );
    divideTriangle( ab, bc, ac, count );
  }
}

function render()
{
  var vertices = [
    vec2( -0.5, -0.5 ),
    vec2(  0,  0.5 ),
    vec2(  0.5, -0.5 )
  ];

  points = [];
  divideTriangle( vertices[0], vertices[1], vertices[2], numTimesToSubdivide);

  gl.bufferSubData( gl.ARRAY_BUFFER, 0, flatten(points) );

  gl.clear( gl.COLOR_BUFFER_BIT );
  gl.drawArrays( gl.TRIANGLES, 0, points.length );
}

window.onload = init;
